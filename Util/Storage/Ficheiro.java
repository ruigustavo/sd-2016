
/**
 * Created by ruigustavo on 21/10/2016.
 */
import java.io.*;

public class Ficheiro {
    private ObjectInputStream	iS;
    private ObjectOutputStream oS;


    public boolean abreLeitura(String nomeDoFicheiro) {
        try {
            iS = new ObjectInputStream(new FileInputStream(nomeDoFicheiro));
            return true;
        }
        catch (FileNotFoundException e) {
            System.out.println("Ficheiro not open/existent. abreLeitura");
            e.printStackTrace();
        }
        catch (SecurityException e) {
            System.out.println("security exception");
            e.printStackTrace();
        }
        catch (StreamCorruptedException e) {
            System.out.println("stream corrupted exception.");
            e.printStackTrace();
        }
        catch (IOException e) {
            System.out.println("io exception.AbreLeitura");
            e.printStackTrace();
        }

        return false;
    }

    public boolean abreEscrita(String nomeDoFicheiro) {
        try {
            oS = new ObjectOutputStream(new FileOutputStream(nomeDoFicheiro));
            return true;
        } catch (FileNotFoundException e) {
            System.out.println("Ficheiro not open/existent. abreEscrita");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("io exception. abreEscrita");
            e.printStackTrace();
        }
        return false;
    }

    public void fechaLeitura() {
        try {
            iS.close();
        }
        catch (Exception e) {
            System.out.println("Nao foi possivel fechar o ficheiro aberto para leitura.");
        }
    }

    public void fechaEscrita() {
        try {
            oS.close();
        }
        catch (Exception e) {
            System.out.println("Nao foi possivel fechar o ficheiro aberto para escrita.");
        }
    }

    public Object leObjecto() {
        try {
            try {
                return iS.readObject();
            }
            catch (IOException e) {
                System.err.println("Unable to read file");
            }
        }
        catch (ClassNotFoundException e) {
            System.out.println("Ficheiro doesn't contain any objects");
        }
        return null;
    }

    public void escreveObjecto(Object o) {
        try {
            oS.reset();
            oS.flush();
            oS.writeObject(o);
            oS.flush();
        }
        catch (Exception e) {
            System.out.println("Nao foi possivel escrever no ficheiro.");
        }
    }

    public void writeObjects(String fileName, Object obj) {

        if (abreEscrita(fileName)) {
            escreveObjecto(obj);
            fechaEscrita();
        }
    }

    public static boolean deleteAttachment(int clientID, String path) {
        String absPath = String.format("files/%d/%s", clientID, path);
        java.io.File f = new java.io.File(absPath);
        System.err.println(absPath);
        return f.delete();
    }

    public static boolean deleteAttachment(String path) {
        String absPath = String.format("files/%s", path);
        java.io.File f = new java.io.File(absPath);
        System.err.println(absPath);
        return f.delete();
    }

    public static byte[] readAttachment(String fileName) throws IOException {
        byte[] data = null;
        long leng;

        if (!fileName.isEmpty()) {
            RandomAccessFile f;

            f = new RandomAccessFile(fileName, "r");
            leng = f.length();
            data = new byte[(int) leng];

            f.read(data);
            f.close();
        }

        return data;
    }
}
