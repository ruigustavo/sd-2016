import java.io.Serializable;

/**
 * Created by roger on 21/10/2016.
 */
public class Bid implements Serializable {

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Bid(float amount, String username) {
        this.amount = amount;
        this.username = username;
    }

    public float getAmount() {
        return amount;
    }

    public String getUsername() {
        return username;
    }

    float amount;

    String username;

}
