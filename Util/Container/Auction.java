import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Vector;

/**
 * Created by roger on 21/10/2016.
 */
public class Auction implements Serializable {

    String title;
    String description;
    String code;
    String username;
    String vencedor;
    boolean active;
    Date deadline;
    Vector<Auction>auctionHistory = new Vector<>();

    public Vector<Auction> getAuctionHistory() {
        return auctionHistory;
    }

    public void setAuctionHistory(Vector<Auction> auctionHistory) {
        this.auctionHistory = auctionHistory;
    }

    public String getVencedor() {
        return vencedor;
    }

    public void setVencedor(String vencedor) {
        this.vencedor = vencedor;
    }

    Date creation;

    public Date getCreation() {
        return creation;
    }

    public void setCreation(Date creation) {

        this.creation = creation;
    }

    int id;
    float amount;
    List<Message> messages;
    List<Bid> bids;

    public boolean getActive(){return active;}

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public String getCode() {
        return code;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Date getDeadline() {
        return deadline;
    }

    public int getId() {
        return id;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public List<Bid> getBids() {
        return bids;
    }
    public String getUsername() {
        return username;
    }


    public void setBids(List<Bid> bids) {
        this.bids = bids;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Auction(String title, String description, String code, Date deadline, int id, String username, float amount) {


        this.title = title;
        this.username = username;
        this.description = description;
        this.code = code;
        this.deadline = deadline;
        this.id = id;
        this.amount = amount;

        active = true;
        messages = new Vector<>();
        bids = new Vector<>();
        active = true;
    }


    @Override
    public String toString() {
        return "Auction{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", code='" + code + '\'' +
                ", username='" + username + '\'' +
                ", deadline='" + deadline + '\'' +
                ", id=" + id +
                ", active="+ active +

                ", messages=" + messages +
                ", bids=" + bids +
                ", Active?" + active +


                ", amount=" + amount +
                '}';
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

}
