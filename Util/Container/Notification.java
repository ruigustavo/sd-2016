

import java.io.Serializable;
import java.util.Date;

/**
 * Created by roger on 29/10/2016.
 */
public class Notification implements Serializable {
    private String text;
    private Date date;
    private boolean checked;

    public Notification(String text, Date date) {
        this.text = text;
        this.date = date;
        checked = false;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked() {
        this.checked = true;
    }
}
