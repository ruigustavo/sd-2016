import java.io.Serializable;

/**
 * Created by roger on 21/10/2016.
 */
public class Message implements Serializable {
    public Message(String user, String text) {
        this.user = user;
        this.text = text;
    }

    public String getUser() {
        return user;
    }

    public String getText() {
        return text;
    }

    String user,text;
}
