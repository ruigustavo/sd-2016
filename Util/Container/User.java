

import java.io.Serializable;
import java.util.ArrayList;


/**
 * Created by roger on 21/10/2016.
 */
public class User implements Serializable{
    private String username,password;
    private boolean isAdmin;
    private int nAuctions;
    private int nWins;

    public int getnWins() {
        return nWins;
    }

    public void setnWins(int nWins) {
        this.nWins += nWins;
    }

    public ArrayList<Notification> getNotifications() {
        return notifications;
    }

    private ArrayList<Notification> notifications;

    public String getUsername() {
        return username;
    }
    public String getIsBanned(){return isBanned;}
    public String getPassword() {
        return password;
    }

    private String isBanned="false";
    public boolean isAdmin(){return isAdmin;}

    public void setnAuctions(int nAuctions) {
        this.nAuctions += nAuctions;
    }

    public void setIsBanned(String isBanned) {
        this.isBanned = isBanned;
    }

    public int getnAuctions() {
        return nAuctions;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.isAdmin= false;
        notifications = new ArrayList<>();
    }

    public void setAdmin() {
        this.isAdmin = true;
    }
}


