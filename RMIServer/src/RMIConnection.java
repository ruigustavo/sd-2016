import java.rmi.RemoteException;

public class RMIConnection {
  private String rmiHost;
  private int rmiPort;
  private String nameBind;
  private RMIServer_I rmiConnection;
  private ConnectionHelper handle;

  public RMIConnection(String rmiHost, int rmiPort, String rmiName) {
      this.rmiHost = rmiHost;
      this.rmiPort = rmiPort;
      this.nameBind = rmiName;
  }

  public RMIServer_I getRmiConnection() {
      try {
          rmiConnection.printTesting();
      } catch (RemoteException e) {
          rmiConnection = null;
      } catch (NullPointerException e) {
      }
      if (rmiConnection == null) {
          handle = new ConnectionHelper(rmiConnection,rmiHost,rmiPort,nameBind);
          handle.start();
          try {
              handle.join();
          } catch (InterruptedException e1) {
              e1.printStackTrace();
          }
          rmiConnection = handle.getRmiConnection();
      }
      return rmiConnection;
  }

}
