import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created by roger on 29/10/2016.
 */
public  class ConnectionHelper extends Thread {
    public RMIServer_I rmiConnection;
    String rmiHost,  nameBind;
    int time = 0,rmiPort;

    ConnectionHelper(RMIServer_I rmiConnection, String rmiHost, int rmiPort, String nameBind) {
        this.rmiHost = rmiHost;
        this.rmiPort = rmiPort;
        this.nameBind = nameBind;
        this.rmiConnection = rmiConnection;
    }

    public RMIServer_I getRmiConnection() {
        return rmiConnection;
    }

    public void run() {
        time = 0;
        boolean get_conn = false;
        System.getProperties().put("java.security.policy", "politicas.policy");
        String name = "rmi://" + rmiHost + ":" + rmiPort + "/" + nameBind;
        System.setProperty("java.rmi.server.hostname", rmiHost);
        while (get_conn == false) {
            try {
                rmiConnection = (RMIServer_I) Naming.lookup(name);
                get_conn = true;
            } catch (NotBoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (RemoteException e) {
                System.err.println("RMIServer not found!");
                time++;
                if (time > 30) {
                    System.out.println("RMIServer is Down");
                }
                try {
                    this.sleep(2000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
