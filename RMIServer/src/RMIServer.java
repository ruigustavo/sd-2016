/**
 * Created by ruigustavo on 21/10/2016.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.text.ParseException;
import java.util.Date;
import java.util.Timer;
import java.util.Vector;

public class RMIServer implements RMIServer_I {
    private static final String rmiConfig = "rmi.txt";
    private static boolean fullBD = false;
    private static String pathPolicy;
    private static int registryPort; //1099
    private static String nameBind;

    private static int number_rmi;
    private static BufferedReader reader = null;

    //public static DataStructure dataStructure;
    public static DataBase dataBase;


    public RMIServer() throws RemoteException {
        super(); // call base class constructor
    }

    public static void main(String argv[]) throws RemoteException {

        InputStreamReader input = new InputStreamReader(System.in);
        reader = new BufferedReader(input);
        if(argv.length==0)
        {
            try{
                System.out.println("1-Primario\n2-Secundario");
                System.out.print("RMI number: ");
                number_rmi = Integer.parseInt(reader.readLine());
            }catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        else
        {
            number_rmi = Integer.parseInt(argv[0]);
        }

/*
        pathPolicy = props.getProperty("pathPolicy");
        registryPort = Integer.parseInt(props.getProperty("registryPort"));
        nameBind = props.getProperty("nameBind");
        */
        new RMIServer().init(number_rmi);
    }

    static class RMIBackupConnection extends Thread {
        private int serverPort;
        private String rmiName;

        RMIBackupConnection(int port, String rmiName) {
            this. serverPort = port;
            this.rmiName = rmiName;
        }

        public void run() {
            InputStreamReader input = new InputStreamReader(System.in);
            reader = new BufferedReader(input);
            String rmiIp;
            int tries = 0;
            try {
                System.out.print("Primary RMI IP: ");
                rmiIp = reader.readLine();
                System.getProperties().put("java.security.policy", "politicas.policy");
                String sec_name = "rmi://" + rmiIp + ":" +  serverPort + "/" + rmiName;
                System.setProperty("java.rmi.server.hostname", rmiIp);


                while (tries < 3) {
                    try {
                        RMIServer_I rmiConnection = (RMIServer_I) Naming.lookup(sec_name);
                        System.out.println("Secondary RMI UP!");
                        tries = 0;
                        while (true) {
                            System.out.println(rmiConnection.testRMI());
                            try {
                                this.sleep(5000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (NotBoundException e) {
                        e.printStackTrace();
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (RemoteException e) {
                        tries++;
                        System.err.println("Cannot Connect, try " + tries);
                        try {
                            this.sleep(1000);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
                try {
                    new RMIServer().main(new String[]{"1"});
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }


    public void init(int n) {
        //RMIServer.projects = new ConcurrentLinkedQueue<>();
        try {
            BufferedReader fR = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream (rmiConfig)));
            pathPolicy = fR.readLine();
            registryPort = Integer.parseInt(fR.readLine());
            nameBind = fR.readLine();
            fR.close();
        }
        catch (Exception e){
            System.out.println("Erro ao abrir ficheiro rmi");
            System.exit(1);
        }


        InputStreamReader input = new InputStreamReader(System.in);
        reader = new BufferedReader(input);
        number_rmi = n;

        if(number_rmi==1)
        {
            try
            {
                RMIServer_I rmi = new RMIServer();
                RMIServer_I stub = (RMIServer_I) UnicastRemoteObject.exportObject(rmi,0);
                Registry regis = LocateRegistry.createRegistry(registryPort);
                regis.rebind(nameBind, stub);
                System.out.println("RMI Server Running!");
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            new RMIBackupConnection(registryPort, nameBind).start();
        }

        System.out.println("RMI server: Current Time = " + new Date());

        System.getProperties().put("java.security.policy", pathPolicy);
        System.setSecurityManager(null);

        // instancia DataStructure (resposible for data handling)
        System.out.println("RMI server: Estrutura de dados pronta!");
        dataBase = new DataBase(fullBD, this);
        Timer t = new Timer();
        Task mTask = new Task(this);
        t.scheduleAtFixedRate(mTask, 0, 30000);

    }



    @Override
    public boolean login(User user) throws RemoteException {
        return dataBase.login(user);
    }
    @Override
    public boolean isAdmin(String user) throws RemoteException {
        return dataBase.isAdmin(user);
    }

    public String printTesting() throws RemoteException {
        return ">>>Testing connection<<<";
    }

    public String testRMI() throws RemoteException
    {
        return "Primary RMI is alive";
    }

    @Override
    public boolean writeMessage(int id, String message, String username) throws RemoteException {
        return dataBase.writeMessage(id, message, username);
    }

    @Override
    public  Vector<Auction> searchAuctionsInvolved(String username) throws RemoteException{
        return dataBase.searchAuctionsInvolved(username);
    }

    @Override
    public boolean register(User user) throws RemoteException {
        return dataBase.registerUser(user);
    }

    @Override
    public boolean createAuction(Auction auction) throws ParseException {
        return dataBase.createAuction(auction);
    }

    @Override
    public boolean cancelAuctionByCode(String code){return dataBase.cancelAuctionByCode(code);}

    @Override
    public Auction searchAuctionById(int id) {
        return dataBase.searchAuctionByID(id);
    }
    @Override
    public boolean banUser(String user){return dataBase.banUser(user);}

    @Override
    public boolean cancelAuctionByUsername(String username){return dataBase.cancelAuctionByUsername(username);}


    @Override
    public Vector<Auction> searchAuctionByCode(String code) {
        return dataBase.searchAuctionByCode(code);
    }
    @Override
    public Vector<User> moreBidsUsers(){return dataBase.moreBidsUsers();}
    @Override
    public Vector<Auction> getAuctions10Days() {return dataBase.getAuctions10Days();}
    @Override
    public void setAdmin(String username) throws RemoteException {
        dataBase.setAdmin(username);
    }

    @Override
    public Vector<String> readUnreadNotifications(String username) throws RemoteException {
        return dataBase.readUnreadNotifications(username);
    }


    @Override
    public void writeMessageNotifications(String s,int id,String username) throws RemoteException {
         dataBase.writeMessageNotifications(s,id,username);
    }


    @Override
    public boolean createBid(int id, float amount, String username) throws RemoteException {
        return dataBase.createBid(id, amount, username);
    }

    public Vector<Auction> searchAuctionByUser(String username) {
        return dataBase.searchAuctionByUser(username);
    }

    @Override
    public boolean editAuction(int id, String action, String content,String username) {
        return dataBase.editAuction(id, action, content,username);
    }
    @Override
    public void setWinners(Auction p){dataBase.setWinners(p);}
    @Override
    public int getAuctionsSize() {
        return dataBase.getAuctionsSize();
    }

    @Override
    public Vector<Auction> getAuctions() throws RemoteException {
        return dataBase.getAuctions();
    }
    @Override
    public Vector<String> getWinnerUsers() throws RemoteException{return dataBase.getWinnerUsers();}
    @Override
    public Vector<User> getUsers() throws RemoteException {
        return dataBase.getUsers();
    }
    @Override
    public void cancelAuctionById(int id) throws RemoteException {
        dataBase.cancelAuctionById(id);
    }



}
