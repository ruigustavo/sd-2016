/**
 * Created by ruigustavo on 21/10/2016.
 */

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.Vector;

public interface RMIServer_I extends Remote
{
    boolean login(User user) throws RemoteException;
    boolean isAdmin(String user) throws RemoteException;
    boolean register(User user) throws RemoteException;

    String testRMI() throws RemoteException;
    String printTesting() throws RemoteException;

    boolean createAuction(Auction auction) throws RemoteException, ParseException;

    Auction searchAuctionById(int id) throws RemoteException;

    Vector<Auction> searchAuctionByCode(String code) throws RemoteException;


    boolean createBid(int id,float amount,String username) throws RemoteException;

    Vector<Auction> searchAuctionByUser(String username) throws RemoteException;

    Vector<Auction> searchAuctionsInvolved(String username) throws RemoteException;

    boolean cancelAuctionByCode(String code)throws RemoteException;

    boolean editAuction (int id,String action,String content,String username) throws RemoteException;
    boolean cancelAuctionByUsername(String username) throws RemoteException;
    boolean writeMessage(int id, String message,String username) throws RemoteException;
    boolean banUser(String user) throws RemoteException;
    int getAuctionsSize() throws RemoteException;
    Vector<String> getWinnerUsers() throws RemoteException;
    void setWinners(Auction p) throws RemoteException;
    Vector<User> getUsers() throws  RemoteException;
    Vector<Auction> getAuctions() throws RemoteException;
    Vector<Auction> getAuctions10Days() throws RemoteException;
    void cancelAuctionById(int id) throws RemoteException;

    Vector<User> moreBidsUsers() throws RemoteException;

    void setAdmin(String username) throws RemoteException;


    Vector<String> readUnreadNotifications(String username) throws RemoteException;


    void writeMessageNotifications(String s, int id, String username) throws  RemoteException;
}
