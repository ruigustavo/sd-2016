import java.rmi.RemoteException;
import java.util.Date;
import java.util.TimerTask;
import java.util.Vector;

/**
 * Created by roger on 26/10/2016.
 */
class Task extends TimerTask{
    static RMIServer_I serverInstance;
    public Task(RMIServer_I rmiServer){serverInstance = rmiServer;}

    @Override
    public void run() {
        try{
            Vector<Auction> auctionsData = serverInstance.getAuctions();
            for(Auction p: auctionsData){
                if((p.getDeadline().before(new Date())) && p.isActive()){
                    serverInstance.cancelAuctionById(p.getId());
                    if(p.getBids()!=null) {
                        serverInstance.setWinners(p);
                    }

                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }
}
