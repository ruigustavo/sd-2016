import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by ruigustavo on 21/10/2016.
 */
public class DataBase {
    private Ficheiro ficheiro;
    private final boolean fullBD;
    private RMIServer_I rmiServer;
    private final String usersFileName = "usersData.dat";
    private final String auctionsFileName = "auctionsData.dat";

    private Vector<User> usersData;

    private Vector<Auction> auctionsAux;

    public int getAuctionsSize() {
        return auctionsData.size();
    }

    private Vector<Auction> auctionsData;


    public DataBase(boolean fullBD, RMIServer_I rmiServer) {
        // Sets up the structure mode
        this.fullBD = fullBD;
        this.rmiServer = rmiServer;
        this.ficheiro = new Ficheiro();

        // Creates an instance of the class that contains the pool of connections
        // criar instancia de bd

        if (fullBD) {
            //BASE DADOS SQL
        } else {
            System.out.println("NO BD Integration Mode (SD)");
            usersData = new Vector<>();
            auctionsData = new Vector<>();
            loadDataFiles();

        }
    }

    private void loadDataFiles() {
        auctionsAux = new Vector<>();
        if (ficheiro.abreLeitura(usersFileName)) {
                usersData = (Vector<User>) ficheiro.leObjecto();
                ficheiro.fechaLeitura();
        } else
            ficheiro.writeObjects(usersFileName, usersData);

        if (ficheiro.abreLeitura(auctionsFileName)) {
                auctionsData = (Vector<Auction>) ficheiro.leObjecto();
                ficheiro.fechaLeitura();

        } else
            ficheiro.writeObjects(auctionsFileName, auctionsData);
    }


    public boolean login(User cliente) {
        loadDataFiles();
        User e = searchUser(cliente.getUsername());
        if(e!=null){
            if(e.getPassword().compareTo(cliente.getPassword())==0)
                if(e.getIsBanned().compareTo("false")==0)
                    return true;
        }
        return false;
    }

    public boolean isAdmin(String username){
        loadDataFiles();
        User e = searchUser(username);
        if(e!=null){
            if(e.isAdmin())
                return true;
        }
        return false;
    }

    public boolean registerUser(User cliente) {
        loadDataFiles();
        if (cliente != null) {
            if (searchUser(cliente.getUsername())==null) {
                synchronized (usersData) {
                    usersData.add(cliente);
                    //grava em ficheiro
                    ficheiro.writeObjects(usersFileName, usersData);
                    return true;
                }
            }
        }
        return false;
    }

    public User searchUser(String user) {
        loadDataFiles();
        for(User e: usersData){
            if(e!=null){
                if(e.getUsername().compareTo(user)==0)
                    return e;
            }
        }
        return null;
    }


    public Auction searchAuctionByID(int id) {
        loadDataFiles();
        for (Auction e : auctionsData) {
            if (e.getId() == id) {
                return e;
            }
        }
        return null;
    }

    public Vector<Auction> searchAuctionByCode(String code){
        loadDataFiles();
        Vector<Auction> aux = new Vector<>();
        Iterator<Auction> itr = auctionsData.iterator();
        Auction auction;
        while (itr.hasNext()) {
            auction = itr.next();
            if (code.compareTo(auction.getCode())==0) {
                synchronized (aux){
                    aux.add(auction);
                }
            }
        }
        return aux;
    }

    public Vector<Auction> searchAuctionByUser(String username){
        loadDataFiles();
        Vector<Auction> aux = new Vector<>();
        for(Auction e: auctionsData){
            if(e.getUsername().compareTo(username)==0){
                aux.add(e);
            }
            for(Bid f: e.getBids()){
                if(f.getUsername().compareTo(username)==0 && !aux.contains(e)){
                    aux.add(e);
                }
            }
        }
        return aux;
    }

    public boolean editAuction(int id,String action,String content,String username) {
        boolean response = false;
        loadDataFiles();
        Iterator<Auction> itr = auctionsData.iterator();
        Auction auction;
        while (itr.hasNext()) {
            auction = itr.next();
            if (id == auction.getId() && username == auction.getUsername()){
                auction.getAuctionHistory().add(auction);
                if(action.compareTo("deadline")==0){
                    try{
                        DateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm", Locale.UK);
                        auction.setDeadline(inputDateFormat.parse(content));
                        response = true;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                else if(action.compareTo("title")==0){
                    auction.setTitle(content);
                    response = true;
                }
                else if(action.compareTo("description")==0){
                    auction.setDescription(content);
                    response = true;
                }
                else if(action.compareTo("amount")==0){
                    auction.setAmount(Float.parseFloat(content)); //SET DO AMOUNT FALTA CRIAR O PARAMETR NA AUCTION ROGERIO NOOB
                    response = true;
                }
            }
        }
        synchronized (auctionsData){
        ficheiro.writeObjects(auctionsFileName, auctionsData);
        }
        return response;
    }

    public boolean writeMessage(int id, String message, String username){
        loadDataFiles();
        Iterator<Auction> itr = auctionsData.iterator();
        Auction auction;
        Message e = new Message(username,message);
        while (itr.hasNext()) {
            auction = itr.next();
            if (id == auction.getId()) {
                auction.getMessages().add(e);
                auctionsAux.add(auction);
                synchronized (auctionsData) {
                    ficheiro.writeObjects(auctionsFileName, auctionsData);
                }
                return true;
            }
        }
        return false;
    }

    public boolean createAuction(Auction auction) throws ParseException {
        loadDataFiles();
        if(auction!= null){
            synchronized (auctionsData){
                auction.setCreation(new Date());
                auctionsData.add(auction);
                ficheiro.writeObjects(auctionsFileName, auctionsData);

            }
            Iterator<User> itr = usersData.iterator();
            User client ;
            int i=0;
            while (itr.hasNext()) {
                client = itr.next();
                if (auction.getUsername().equals(client.getUsername())) {
                    usersData.get(i).setnAuctions(1);
                    ficheiro.writeObjects(usersFileName, usersData);
                }
                i++;
            }
            return true;
        }
        return false;
    }
    public boolean cancelAuctionByCode(String code){
        loadDataFiles();
        Iterator<Auction> itr = auctionsData.iterator();
        Auction auction;
        while (itr.hasNext()) {
            auction = itr.next();
            if (Integer.parseInt(code) == Integer.parseInt(auction.getCode())) {
                auction.setActive(false);
                return true;
            }
        }
        return false;
    }
    public boolean cancelAuctionByUsername(String username){
        Iterator<Auction> itr = auctionsData.iterator();
        Auction auction;
        boolean flag = false;
        while (itr.hasNext()) {
            auction = itr.next();
            if (username.compareTo(auction.getUsername())==0) {
                auction.setActive(false);
                flag = true;
            }
        }
        if(flag)return true;
        else return false;
    }
    public Vector<User> moreBidsUsers(){
        loadDataFiles();
        sortMoreBids(usersData);
        Vector<User> moreBidsUsers = new Vector<>();
        int i = 0;
        while(i<10&&i<usersData.size()) {
            moreBidsUsers.add(usersData.get(i));
            i++;
        }
        return moreBidsUsers;

    }

    public void sortMoreBids(List<User> moreBidsUsers) {
        Collections.sort(moreBidsUsers, new Comparator<User>(){
            @Override
            public int compare(User o1, User o2) {
                if(o1.getnAuctions()<o2.getnAuctions()) {
                    return 1;
                }
                else if(o1.getnAuctions()>o2.getnAuctions()){
                    return -1;
                }
                return 0;
            }
        });
    }
    private void cancelBids(String username){
        synchronized(auctionsData) {
            float valor = 0;
            boolean flag;

            Vector<Bid> bidsToRemove = new Vector<>();
            for (Auction e : auctionsData) {
                flag=false;
                sortBidAmount(e.getBids());
                String user = e.getBids().get(e.getBids().size()-1).getUsername();
                for (Bid f : e.getBids()) {
                    if (f.getUsername().compareTo(username) == 0 && !flag) {
                        valor = f.getAmount();
                        flag=true;
                    }
                    if(flag){
                        bidsToRemove.add(f);
                    }

                }
                while(!bidsToRemove.isEmpty()){
                    e.getBids().remove(bidsToRemove.get(0));
                    bidsToRemove.remove(0);
                }
                if(flag){
                    e.setAmount(valor+1);
                    ficheiro.writeObjects(usersFileName, usersData);
                    ficheiro.writeObjects(auctionsFileName,auctionsData);
                    createBid(e.getId(),valor,user);
                    writeMessage(e.getId(),"We're sorry. The user "+username+" has been banned. Bids have been updated", "Ibei support");
                }
            }

        }
    }
    private void sortBidAmount(List<Bid> bids) {
        Collections.sort(bids, new Comparator<Bid>(){
            @Override
            public int compare(Bid o1, Bid o2) {
                if(o1.getAmount()<o2.getAmount()) {
                    return 1;
                }
                else if(o1.getAmount()>o2.getAmount()){
                    return -1;
                }
                return 0;
            }
        });
    }

    public boolean banUser(String userToBan){
        searchAuctionByID(0);
        loadDataFiles();
        Iterator<User> itr = usersData.iterator();
        User client;
        while (itr.hasNext()) {
            client = itr.next();
            if (userToBan.equals(client.getUsername())) {
                client.setIsBanned("true");
                cancelAuctionByUsername(userToBan);
                cancelBids(userToBan);
                ficheiro.writeObjects(usersFileName, usersData);
                ficheiro.writeObjects(auctionsFileName,auctionsData);
                return true;
            }

        }
        return false;

    }

    public  Vector<Auction> searchAuctionsInvolved(String username){
        Vector<Auction> aux = new Vector<>();
        Iterator<Auction> itr = auctionsAux.iterator();
        Auction auction;
        while (itr.hasNext()) {
            auction = itr.next();
            int aux2 = auction.getMessages().size();
            if (username.compareTo(auction.getUsername()) == 0 || username.compareTo(auction.getBids().get(aux2).getUsername()) == 0) {
                synchronized (aux) {
                    aux.add(auction);
                    auctionsAux.remove(auction);
                }
            }
        }
        return aux;
    }

    public boolean createBid(int id, float amount,String username){
        loadDataFiles();
        for(Auction auction: auctionsData)
            if (id == auction.getId() && auction.isActive()) {
                if(auction.getAmount() > amount){
                    auction.setAmount(amount);
                    auction.getBids().add(new Bid(amount,username));
                    ficheiro.writeObjects(auctionsFileName, auctionsData);

                    return true;
                }
            }
        return false;
    }


    public Vector<Auction> getAuctions() {
        return auctionsData;
    }
    public Vector<User> getUsers(){return usersData;}
    public boolean cancelAuctionById(int id) {
        loadDataFiles();
        for(Auction e: auctionsData){
            if(id == e.getId()){
                e.setActive(false);
                ficheiro.writeObjects(auctionsFileName, auctionsData);
                return true;
            }
        }
        return false;
    }


    public void setWinners(Auction p){
        System.out.println("WTF???" + p.getBids().get(p.getBids().size() - 1).getUsername());
        p.setVencedor(p.getBids().get(p.getBids().size() - 1).getUsername());
        for (User u : usersData) {
            if (u.getUsername().compareTo(p.getBids().get(p.getBids().size() - 1).getUsername()) == 0) {
                u.setnWins(1);
            }
        }
        ficheiro.writeObjects(usersFileName, usersData);
    }
    public boolean setAdmin(String username) {
        for(User e: usersData){
            if(e.getUsername(). compareTo(username)==0){
                e.setAdmin();
                ficheiro.writeObjects(usersFileName, usersData);
                return true;
            }
        }
        return false;
    }


    public Vector<String> readUnreadNotifications(String username) {
        Vector<String> aux= new Vector<>();
        User e = searchUser(username);
        if(e!=null){
            for(Notification f: e.getNotifications()){
                if(!f.isChecked() && f!=null){
                    f.setChecked();
                    aux.add(f.getText());
                    ficheiro.writeObjects(usersFileName, usersData);
                }
            }
        }
        return aux;
    }

    public void writeMessageNotifications(String s, int id,String usernameDaNovaMensagem) {

        ArrayList<String> aux = new ArrayList<>();
        Auction e = searchAuctionByID(id);
        if (e != null) {
            if (e.getUsername().compareTo(usernameDaNovaMensagem) != 0)
                aux.add(e.getUsername());
            for (Message f : e.getMessages()) {
                if (!aux.contains(f.getUser()) && f.getUser().compareTo(usernameDaNovaMensagem) != 0) {
                    aux.add(f.getUser());
                }
            }
        }
        for (String f : aux) {
            User g = searchUser(f);
            if (g != null) {
                g.getNotifications().add(new Notification(s, new Date()));
            }
        }
        ficheiro.writeObjects(usersFileName, usersData);
    }

    public Vector<Auction> getAuctions10Days(){
        loadDataFiles();
        Vector<Auction>auctions10Days = new Vector<>();
        Date dataAtual = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(dataAtual);
        cal.add(Calendar.DATE, -10);
        Date dateBefore10Days = cal.getTime();
        System.out.println(dateBefore10Days);
        for(Auction a: auctionsData){
            System.out.println(a.getTitle());
            System.out.println(a.getCreation());
            if(a.getCreation().before(dataAtual)&& a.getCreation().after(dateBefore10Days)){
                auctions10Days.add(a);
            }
        }
        return auctions10Days;
    }

    public Vector<String> getWinnerUsers(){
        loadDataFiles();
        Vector<String>winnerUsers = new Vector<>();
        int i=0;
        sortWinnerUsers(usersData);
        for(User a:usersData){
            System.out.println("USer: "+a.getUsername() + "Auctions Won: "+ a.getnWins());
            if(a.getnWins()>0){
                System.out.println("Olá???" + a.getnWins());
                winnerUsers.add(a.getUsername());
                i++;
            }
            if(i==10)break;
        }
        return winnerUsers;
    }

    public void sortWinnerUsers(List<User> winnerUsers) {
        Collections.sort(winnerUsers, new Comparator<User>(){
            @Override
            public int compare(User o1, User o2) {
                if(o1.getnWins()<o2.getnWins()) {
                    return 1;
                }
                else if(o1.getnWins()>o2.getnWins()){
                    return -1;
                }
                return 0;
            }
        });
    }
}