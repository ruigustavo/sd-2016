/**
 * Created by ruigustavo on 21/10/2016.
 */


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class Server {

    private static final String svConfig = "Server/tcpserver.txt";
    private Listener listener;
    private static RMIServer_I rmiServer;
    private static String rmiServerIP;
    private static int registryPort; //1099
    private static String nameBind;
    UDPSocket udpSocket;

    public int getServerPort() {
        return serverPort;
    }


    public int getSecondaryPort() {
        return secondaryServerPort;
    }

    private int serverPort;
    private int secondaryServerPort;
    private String secondaryServerIp;

    public String getSecondaryServerIp() {
        return secondaryServerIp;
    }

    public int getnUsersOnline() {
        return nUsersOnline;
    }

    public void setnUsersOnline(int nUsersOnline) {
        this.nUsersOnline = nUsersOnline;
    }

    private int nUsersOnline;

    public int getnUsersOnlineOtherServer() {
        return nUsersOnlineOtherServer;
    }

    public void setnUsersOnlineOtherServer(int nUsersOnlineOtherServer) {
        this.nUsersOnlineOtherServer = nUsersOnlineOtherServer;
    }

    private int nUsersOnlineOtherServer;


    public Server(){
        try {
            BufferedReader fR = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream ("tcpserver.txt")));


            rmiServerIP = fR.readLine();
            registryPort = Integer.parseInt(fR.readLine());
            nameBind = fR.readLine();
            serverPort = Integer.parseInt(fR.readLine());
            secondaryServerIp = fR.readLine();
            secondaryServerPort = Integer.parseInt(fR.readLine());
            fR.close();
        }
        catch (Exception e){
            System.out.println(e);
            System.out.println("Erro ao abrir ficheiro tcpserver.txt");
            System.exit(1);
        }
        nUsersOnline =0;
        InputStreamReader input = new InputStreamReader(System.in);
        RMIConnection rmi_conn = new RMIConnection(rmiServerIP,registryPort,nameBind);

                listener = new Listener(serverPort, this,rmi_conn);
                udpSocket = new UDPSocket(this);


        System.out.println("Trying to connect to DB");
        try {
            rmiServer = (RMIServer_I) Naming.lookup("rmi://" + rmiServerIP + ":" + registryPort + "/" + nameBind);
            System.out.println("Connected to Database!");
        }
        catch (RemoteException e) {
            System.err.println("RMI failed to connect to registry");
        }
        catch (NotBoundException e) {
            System.out.println("RMI Failed attempt to connect");
        }
        catch (IOException e) {
            System.out.println("RMI Failed attempt to connect EX");
        }
    }

    public static void main(String args[]) throws InterruptedException {

/*
        rmiServerIP = props.getProperty("rmiServerIP");
        registryPort = Integer.parseInt(props.getProperty("registryPort"));
        nameBind = props.getProperty("nameBind");
*/
        new Server();
    }

}
