import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * Created by roger on 28/10/2016.
 */
public class UDPSocket extends  Thread{
    DatagramSocket serverSocket;
    private Server server;

    public UDPSocket(Server server){
        this.server = server;
        try {
            serverSocket =  new DatagramSocket(server.getServerPort());
            this.start();

        } catch (SocketException e) {
            e.printStackTrace();
        }

    }

    public void run() {
        try {
            byte[] receiveData = new byte[4];
            while(true) {
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                String s = String.valueOf(server.getnUsersOnline());
                DatagramPacket dp = new DatagramPacket(s.getBytes() , s.getBytes().length , InetAddress.getByName(server.getSecondaryServerIp()) , server.getSecondaryPort());
                serverSocket.send(dp);
                serverSocket.receive(receivePacket);
                String sentence = new String(receivePacket.getData(),0,receivePacket.getLength());
                server.setnUsersOnlineOtherServer(Integer.parseInt(sentence));
                this.sleep(5000);
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally
        {
            if( serverSocket != null )
                serverSocket.close() ;
        }
    }

}
