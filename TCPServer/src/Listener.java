/**
 * Created by ruigustavo on 21/10/2016.
 */

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by roger on 18/10/2016.
 */
public class Listener extends Thread {
    private int serverPort;
    private Server server;
    private RMIConnection rmi_conn;

    public Listener(int serverPort,Server server,RMIConnection rmi_conn){
        this.serverPort = serverPort;
        this.server = server;
        this.rmi_conn = rmi_conn;
        this.start();
    }

    @Override
    public void run() {
        System.out.println("Listening on " + serverPort);
        ServerSocket listensocket = null;
        try {
            listensocket = new ServerSocket(serverPort);
        } catch (IOException e) {
        }
        while (true) {
            Socket clientSocket = null;
            try {
                clientSocket = listensocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("CLIENT_SOCKET (created at accept())="+clientSocket);

            new Connection(clientSocket,this.server, this.rmi_conn);
        }
    }

    public int getServerPort(){
        return serverPort;
    }
}
