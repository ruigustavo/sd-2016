/**
 * Created by ruigustavo on 21/10/2016.
 */

import java.io.*;
import java.net.Socket;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by roger on 18/10/2016.
 */

//= Thread para tratar de cada canal de comunicação com um cliente
class Connection extends Thread {

    PrintWriter outToServer;
    BufferedReader inFromServer = null;

    RMIServer_I rmiConnection;
    RMIConnection rmi_conn;

    Socket clientSocket;
    private Server server;

    private User user; // trocar para User user = null;
    private HashMap response;


    public Connection(Socket aClientSocket, Server server, RMIConnection rmi_conn) {

        try {
            this.server = server;
            clientSocket = aClientSocket;
            inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            outToServer = new PrintWriter(clientSocket.getOutputStream(), true);

            this.rmi_conn = rmi_conn;
            this.rmiConnection = rmi_conn.getRmiConnection();

            this.user = null;
            this.response = new HashMap();
            this.start();
        } catch (IOException e) {
            System.out.println("Connection:" + e.getMessage()+ "\n");
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            HashMap data = new HashMap();
            String aux_in;
            while (true) {
                while (user == null) {
                    data.clear();
                    aux_in = inFromServer.readLine();
                    String[] aux_split_1 = aux_in.split(",");
                    for (String e : aux_split_1) {
                        String[] aux_split_2 = e.split(":");
                        data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
                    }
                    if ("login".compareTo((String) data.get("type")) == 0) {
                        //verificar se e feito o login
                        response.put("type","login");

                        try {
                            if (rmiConnection.login(new User((String) data.get("username"), (String) data.get("password")))) {
                                response.put("ok", "true");
                                response.put("msg", "Welcome to iBei");
                                user = new User((String) data.get("username"), (String) data.get("password"));
                                server.setnUsersOnline(server.getnUsersOnline() + 1);
                                if (rmiConnection.isAdmin((String) data.get("username"))) {
                                    user.setAdmin();
                                    response.put("admin", "true");
                                    response.put("msg2", "Welcome to iBei. You have access to Administration Interface");
                                } else {
                                    response.put("admin", "false");
                                }
                            } else {
                                response.put("ok", "false");
                                response.put("msg", "Try again.");
                            }
                        }catch (RemoteException e) {
                            rmiConnection = rmi_conn.getRmiConnection();
                        }
                    }

                    else if ("register".compareTo((String)data.get("type"))==0){
                        response.put("type","register");
                        try {
                            if (rmiConnection.register(new User((String) data.get("username"), (String) data.get("password")))) {
                                if (data.get("admin") != null) {
                                    if ("true".compareTo((String) data.get("admin")) == 0) {
                                        rmiConnection.setAdmin((String) data.get("username"));
                                        response.put("admin", "true");
                                    } else {
                                        response.put("admin", "false");
                                    }
                                }
                                response.put("ok", "true");
                            } else {
                                response.put("ok", "false");
                            }
                        }catch (RemoteException e) {
                            rmiConnection = rmi_conn.getRmiConnection();
                        }
                    }
                    outToServer.println(response.toString().replace("=",":").substring(1,response.toString().length()-1));
                    outToServer.flush();
                    response.clear();
                }


                while (true) {
                    data.clear();
                    data.put("type","null");
                    aux_in = inFromServer.readLine();
                    if(aux_in!=null){
                        String[] aux_split_1 = aux_in.split(",");
                        for (String e1 : aux_split_1) {
                            String[] aux_split_2 = e1.split(":");
                            data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
                        }
                    }


                    if ("create_auction".compareTo((String) data.get("type")) == 0) {
                        response.put("type", "create_auction");
                        try {
                            DateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm", Locale.UK);
                            Auction aux = new Auction((String) data.get("title"), (String) data.get("description"), (String) data.get("code"), inputDateFormat.parse((String) data.get("deadline")), rmiConnection.getAuctionsSize(), user.getUsername(), Integer.parseInt((String) data.get("amount")));

                            if (rmiConnection.createAuction(aux))
                                response.put("ok", "true");

                            else {
                                response.put("ok", "false");
                            }
                        }catch (RemoteException e) {
                            rmiConnection = rmi_conn.getRmiConnection();
                        }
                    } else if ("detail_auction".compareTo((String) data.get("type")) == 0) {
                        response.put("type", "detail_auction");
                        try {
                            Auction e = rmiConnection.searchAuctionById(Integer.parseInt((String) data.get("id")));
                            if (e != null) {
                                response.put("title", e.getTitle());
                                response.put("description", e.getDescription());
                                response.put("deadline", e.getDeadline().toString().replace(":", "-"));
                                response.put("messages_count", String.valueOf(e.getMessages().size()));
                                response.put("code", String.valueOf(e.getCode()));
                                for (int i = 0; i < e.getMessages().size(); i++) {

                                    response.put("messages_" + i + "_user", e.getMessages().get(i).getUser());
                                    response.put("messages_" + i + "_text", e.getMessages().get(i).getText());

                                }
                                response.put("bids_count", String.valueOf(e.getBids().size()));

                                for (int i = 0; i < e.getBids().size(); i++) {

                                    response.put("bids_" + i + "_user", e.getBids().get(i).getUsername());
                                    response.put("bids_" + i + "_amount", String.valueOf(e.getBids().get(i).getAmount()));
                                }
                            }
                        }catch (RemoteException e) {
                            rmiConnection = rmi_conn.getRmiConnection();
                        }
                    } else if (("search_auction".compareTo((String) data.get("type")) == 0)) {
                        response.put("type", "search_auction");
                        try {
                            Vector<Auction> auxVect = rmiConnection.searchAuctionByCode((String) data.get("code"));


                            response.put("items_count", String.valueOf(auxVect.size()));

                            for (int i = 0; i < auxVect.size(); i++) {
                                response.put("items_" + i + "_code", String.valueOf(auxVect.get(i).getCode()));
                                response.put("items_" + i + "_title", auxVect.get(i).getTitle());
                                response.put("items_" + i + "_id", String.valueOf(auxVect.get(i).getId()));
                            }
                        }catch (RemoteException e) {
                            rmiConnection = rmi_conn.getRmiConnection();
                    }


                    } else if ("bid".compareTo((String) data.get("type")) == 0) {
                        response.put("type", "bid");
                        try{
                        if (rmiConnection.createBid(Integer.parseInt((String) data.get("id")), Float.parseFloat((String) data.get("amount")), user.getUsername())) {
                            response.put("ok", "true");
                        }else {
                            response.put("ok", "false");
                        }}catch (RemoteException e) {
                            rmiConnection = rmi_conn.getRmiConnection();
                        }
                    } else if (("my_auctions".compareTo((String) data.get("type")) == 0)) {
                        response.put("type", "my_auctions");
                        try {
                            Vector<Auction> auxVect = rmiConnection.searchAuctionByUser(user.getUsername());

                            response.put("items_count", String.valueOf(auxVect.size()));

                            for (int i = 0; i < auxVect.size(); i++) {
                                response.put("items_" + i + "_code", String.valueOf(auxVect.get(i).getCode()));
                                response.put("items_" + i + "_title", auxVect.get(i).getTitle());
                                response.put("items_" + i + "_id", String.valueOf(auxVect.get(i).getId()));
                            }
                        }catch (RemoteException e) {
                            rmiConnection = rmi_conn.getRmiConnection();
                        }
                    }


                    else if (("edit_auction".compareTo((String) data.get("type")) == 0)) {
                        response.put("type", "edit_auction");
                        ArrayList<String> action = new ArrayList<>();
                        int i=0,cont=0;
                        if(data.get("amount")!=null){
                            action.add("amount");
                            i++;
                        }
                        if(data.get("code")!=null){
                            action.add("code");
                            i++;
                        }
                        if(data.get("deadline")!=null){
                            action.add("deadline");
                            i++;
                        }
                        if(data.get("title")!=null){
                            action.add("title");
                            i++;
                        }
                        if(data.get("description")!=null){
                            action.add("description");
                        }
                        try{
                        for(String e: action){
                            if (rmiConnection.editAuction(Integer.parseInt((String) data.get("id")), e, (String) data.get(e),user.getUsername())){
                                cont++;
                            }
                        }
                        if(cont>=i/2)
                        {
                            response.put("ok", "true");
                        } else {
                            response.put("ok", "false");
                        }
                        }catch (RemoteException e) {
                            rmiConnection = rmi_conn.getRmiConnection();
                        }
                    } else if ("message".compareTo((String) data.get("type")) == 0) {
                        response.put("type", "message");
                        try{
                        if (rmiConnection.writeMessage(Integer.parseInt((String) data.get("id")), (String) data.get("text"), user.getUsername()) == true) {
                            response.put("ok", "true");
                            rmiConnection.writeMessageNotifications(("type : notification_message , id :"+ data.get("id")+", user : "+user.getUsername()+ ", text : "+data.get("text")),Integer.parseInt((String) data.get("id")),user.getUsername() );
                        } else {
                            response.put("ok", "false");
                        }
                        }catch (RemoteException e) {
                            rmiConnection = rmi_conn.getRmiConnection();
                        }
                    } else if ("online_users".compareTo((String) data.get("type")) == 0) {
                        response.put("type", "online_users");
                        response.put("users_count", String.valueOf(server.getnUsersOnline() + server.getnUsersOnlineOtherServer()));
                    }


                    else if("cancel_auctionByCode".compareTo((String) data.get("type"))==0) {

                        response.put("type", "cancel_auctionByCode");
                        try{
                        if (rmiConnection.cancelAuctionByCode((String) data.get("code")))
                            response.put("ok", "true");
                        else {
                            response.put("ok", "false");
                        }
                        }catch (RemoteException e) {
                            rmiConnection = rmi_conn.getRmiConnection();
                        }
                    }
                    else if("statistics".compareTo((String) data.get("type"))==0){
                        response.put("type","statistics");
                        try{
                        Vector<User> moreBidsUsers = rmiConnection.moreBidsUsers();
                        response.put("sizeMoreBidsUsers",String.valueOf(moreBidsUsers.size()));
                        for(int i=0;i<moreBidsUsers.size();i++){
                            response.put("more_bids_users"+i,moreBidsUsers.get(i).getUsername());

                        }
                        Vector<Auction> auctions10Days = rmiConnection.getAuctions10Days();
                        response.put("sizeAuctions10Days",String.valueOf(auctions10Days.size()));
                        int i=0;
                        for(Auction a: auctions10Days){
                            response.put("auctions_10_days"+i,a.getTitle());
                            i++;
                        }
                        Vector<String> winnerUsers = rmiConnection.getWinnerUsers();
                        response.put("sizeWinnerUsers",String.valueOf(winnerUsers.size()));
                        int j=0;
                        for(String s: winnerUsers){
                            response.put("winner_users"+j,s);
                            j++;
                        }
                        }catch (RemoteException e) {
                            rmiConnection = rmi_conn.getRmiConnection();
                        }
                    }
                    else if("banUser".compareTo((String) data.get("type"))==0){
                        response.put("type","banUser");
                        try{
                        if(rmiConnection.banUser((String)data.get("userToBan"))){
                            response.put("msg","User banned");
                        }
                        else
                            response.put("msg","User cannot be banned");
                        }catch (RemoteException e) {
                            rmiConnection = rmi_conn.getRmiConnection();
                        }
                    }
                    else if("testServer".compareTo((String) data.get("type"))==0){
                        Date dataAtual = new Date();
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(dataAtual);
                        cal.add(Calendar.DATE, 1);
                        Date deadLine = cal.getTime();
                        try{
                        Auction auction = new Auction("Teste", "Testar o servidor", "0000", deadLine, rmiConnection.getAuctionsSize(), user.getUsername(), 100);
                        if(rmiConnection.createAuction(auction)){
                            if(rmiConnection.createBid(auction.getId(),50,auction.getUsername())){
                                Auction teste = rmiConnection.searchAuctionById(auction.getId());
                                if(auction.getTitle().compareTo(teste.getTitle())==0 && teste.getBids().get(0).getAmount() == 50)
                                    response.put("test","ok");
                                else response.put("test","failed");
                            }
                            else response.put("test","failed");
                        }
                        else response.put("test","failed");}catch (RemoteException e) {
                            rmiConnection = rmi_conn.getRmiConnection();
                        }
                    }
                    else if("notification".compareTo((String) data.get("type"))==0){
                        response.put("type", "notification");
                        Vector<String> auxVect = rmiConnection.readUnreadNotifications(user.getUsername());
                        response.put("notification_count", String.valueOf(auxVect.size()));

                        for (int i = 0; i < auxVect.size(); i++) {
                            response.put("notification_" + i + "_text", auxVect.get(i));
                        }
                    }



                    outToServer.println(response.toString().replace("=",":").substring(1,response.toString().length()-1));
                    outToServer.flush();
                    response.clear();
                }
            }
        } catch (EOFException e) {
            System.out.println("EOF:" + e);
        } catch (IOException e) {
            System.out.println("IO:" + e);
            if (user != null){
                server.setnUsersOnline(server.getnUsersOnline()-1);
            }
        } catch(ParseException e) {
            e.printStackTrace();
        }
    }
}



