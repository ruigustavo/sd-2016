<%--
  Created by IntelliJ IDEA.
  User: roger
  Date: 04/12/2016
  Time: 18:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>iBei</title>
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="css/palette.css">
  </head>

  <body>
    <header class="w3-container default-primary-color">
      <h3 class="text-primary-color">Authentication</h3>
    </header>
    <div class="w3-container">
      <form class="w3-form" method="post" action="HelloWorldAction">
        <div class="w3-input-group">
          <label>E-mail</label>
          <input class="w3-input" name="email" type="text" required/>
        </div>
        <div class="w3-input-group">
          <label>Password</label>
          <input class="w3-input" name="password" type="password" required/>
        </div>
        <form action="hello" method="post">
          <input type="hidden" name="action" value="login"/>
          <button type="submit" class="w3-btn dark-primary-color">Login</button>
        </form>


      </form>
    </div>

  </body>
</html>
