package actions;

import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public class HelloWorldAction implements SessionAware {
    private String error;
    private Map<String,Object> session;

    public String execute() throws Exception {
        System.out.println("FODASSE");
        return "success";
    }

    public String getName() {
        return (String) session.get("username");
    }
    public String getError(){
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public void setSession(Map<String, Object> map) {

    }
}