package actions;

import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

/**
 * Created by roger on 04/12/2016.
 */

public class LoginAction implements SessionAware {
    private String username = null, password = null;
    private Map<String, Object> session;


    public String logged(){
        if(session.containsKey("username")){
            return "logged";
        }
        return "notlogged";
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public void setSession(Map<String, Object> map) {
        this.session = map;
    }
}
