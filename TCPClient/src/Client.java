/**
 * Created by ruigustavo on 21/10/2016.
 */

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;

/**
 * Created by roger on 18/10/2016.
 */
public class Client {
    PrintWriter outToServer;
    BufferedReader inFromServer = null;
    private static BufferedReader reader = null;
    Socket socket = null;
    private boolean isLogged;
    private boolean check;
    private int serverPort;
    private String serverIp;

    private boolean isAdminLogged;

    public Client(){

        try {
            socket = new Socket();

            try {
                BufferedReader fR = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream ("client.txt")));
                serverIp = fR.readLine();
                serverPort = Integer.parseInt(fR.readLine());
                fR.close();
            }
            catch (Exception e){
                System.out.println(e);
                System.out.println("Erro ao abrir ficheiro client.txt");
                System.exit(1);
            }

            socket.connect(new InetSocketAddress(serverIp, serverPort));
            // create streams for writing to and reading from the socket
            inFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            outToServer = new PrintWriter(socket.getOutputStream(), true);
            InputStreamReader input = new InputStreamReader(System.in);
            reader = new BufferedReader(input);
            isLogged = false;
            int choice;
            while(true) {
                try {
                    while (!isLogged) {
                        System.out.println("Choose one:\n\t1-login\n\t2-registerUser\n\t3-close");
                        choice = Integer.parseInt(reader.readLine());
                        switch (choice) {
                            case 1:
                                login();
                                break;
                            case 2:
                                register();
                                break;
                        }
                    }
                    while (true) {
                        unreadNotifications();
                        if (isAdminLogged) {
                            System.out.println("Choose one:\n\t1-Create auction\n\t2-Search auctions by code\n\t3-Detail auction by id\n" +

                                    "\t4-Bid\n\t5-My activity\n\t6-Edit auction\n\t7-Write in mural\n\t8-List Online Users\n\t9-Cancel auction\n\t10-See statistics\n\t11-Ban User\n\t12-Test Server");

                            choice = Integer.parseInt(reader.readLine());
                            switch (choice) {
                                case 1:
                                    createAuction();
                                    break;
                                case 2:
                                    searchAuction();
                                    break;
                                case 3:
                                    detailAuction();
                                    break;
                                case 4:
                                    bid();
                                    break;
                                case 5:
                                    userActivity();
                                    break;
                                case 6:
                                    editAuction();
                                    break;
                                case 7:
                                    writeMural();
                                    break;
                                case 8:
                                    onlineUsers();
                                    break;
                                case 9:
                                    cancelAuctionByCode();
                                    break;
                                case 10:
                                    statistics();
                                    break;
                                case 11:
                                    banUser();
                                    break;
                                case 12:
                                    testServer();

                            }
                        } else {
                            System.out.println("Choose one:\n\t1-Create auction\n\t2-Search auctions by code\n\t3-Detail auction by id\n" +
                                    "\t4-Bid\n\t5-My activity\n\t6-Edit auction1n\n\t7-Write in mural\n\t8-List Online Users\n");
                            choice = Integer.parseInt(reader.readLine());
                            switch (choice) {
                                case 1:
                                    createAuction();
                                    break;
                                case 2:
                                    searchAuction();
                                    break;
                                case 3:
                                    detailAuction();
                                    break;
                                case 4:
                                    bid();
                                    break;
                                case 5:
                                    userActivity();
                                    break;
                                case 6:
                                    editAuction();
                                    break;
                                case 7:
                                    writeMural();
                                    break;
                                case 8:
                                    onlineUsers();
                                    break;
                            }

                        }
                    }
                } catch (Exception e) {
                }

            }

        } catch (IOException e) {
            if (inFromServer == null)
                System.out.println("\nUsage: java TCPClient <host> <port>\n");
            System.out.println(e.getMessage());
        } finally {
            try {
                inFromServer.close();
            } catch (Exception e) {
            }
        }
    }

    private void unreadNotifications() {
        HashMap data = new HashMap();
        String aux_in;
        try {
            data.put("type", "notification");
            outToServer.println(data.toString().replace("=",":").substring(1,data.toString().length()-1));
            outToServer.flush();
            data.clear();

            aux_in = inFromServer.readLine();
            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }
            if ("notification".compareTo((String) data.get("type")) == 0) {
                int nC = Integer.parseInt((String) data.get("notification_count"));

                System.out.println("\nNotifications Count: " + nC);

                for (int i = 0; i < nC; i++) {
                    String aux_in2 = (String) data.get("notification_"+i+"_text");
                    String[] aux_split_3 = aux_in2.split(",");
                    for (String e1 : aux_split_3) {
                        String[] aux_split_4 = e1.split(":");
                        data.put(aux_split_4[0].trim(), aux_split_4[1].trim());
                    }
                    System.out.println("Notification: "+ i);
                    System.out.println("Id: "+data.get("id"));
                    System.out.println("User: "+data.get("user"));
                    System.out.println("Message:"+data.get("text"));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void login() {
        HashMap data = new HashMap();
        String aux_in;
        try {
            String username, password;
            System.out.println("LOGIN");
            System.out.println("Username:");
            username = reader.readLine();
            System.out.println("Password:");
            password = reader.readLine();
            data.put("type", "login");
            data.put("username", username);
            data.put("password", password);

            outToServer.println(data.toString().replace("=",":").substring(1,data.toString().length()-1));

            outToServer.flush();
            data.clear();

            aux_in = inFromServer.readLine();

            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }

            if ("login".compareTo((String) data.get("type")) ==0 && "true".compareTo((String)data.get("ok"))==0) {
                isLogged = true;
                if("true".compareTo((String) data.get("admin"))==0){
                    isAdminLogged = true;
                    System.out.println((String) data.get("msg2"));
                }
                else {
                    System.out.println((String) data.get("msg"));
                }
            }else {
                System.out.println((String) data.get("msg"));
            }
            //successful login


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void register(){
        HashMap data = new HashMap();

        String aux_in;

        try {
            String username, password;
            System.out.println("REGISTER");
            System.out.println("Username:");
            username = reader.readLine();
            System.out.println("Password:");
            password = reader.readLine();
            System.out.println("Acesso de Administrador? (y/n):");
            String isAdminTmp = reader.readLine();

            data.put("type", "register");
            data.put("username", username);
            data.put("password", password);


            if(isAdminTmp.compareTo("y")==0){
               data.put("admin", "true");
            }
            else if(isAdminTmp.compareTo("n")==0){
                data.put("admin", "false");
            }

            outToServer.println(data.toString().replace("=",":").substring(1,data.toString().length()-1));
            outToServer.flush();
            data.clear();

            aux_in = inFromServer.readLine();
            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }
            if ("register".compareTo((String) data.get("type")) ==0 && "true".compareTo((String)data.get("ok"))!=0) {
                System.out.println("Try again.");
                register();
            }
            System.out.println("Success"); //successful registerUser

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createAuction(){
        HashMap data = new HashMap();
        String aux_in;
        try {
            data.put("type", "create_auction");
            System.out.println("Code:");
            String text = reader.readLine();
            data.put("code", text);
            System.out.println("Title:");
            text = reader.readLine();
            data.put("title", text);
            System.out.println("Description:");
            text = reader.readLine();
            data.put("description", text);
            System.out.println("Deadline(yyyy-MM-dd HH-mm):");
            text = reader.readLine();
            data.put("deadline", text);
            System.out.println("Amount:");
            text = reader.readLine();
            data.put("amount", text);

            outToServer.println(data.toString().replace("=",":").substring(1,data.toString().length()-1));
            outToServer.flush();
            outToServer.flush();
            data.clear();

            aux_in = inFromServer.readLine();
            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }

            if ("create_auction".compareTo((String) data.get("type")) ==0 && "true".compareTo((String)data.get("ok"))==0) {
                System.out.println("Auction Created.");
            }else
                System.out.println("ERROR CREATE AUCTION");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void detailAuction(){
        HashMap data = new HashMap();
        String aux_in;
        try {
            data.put("type", "detail_auction");
            System.out.println("DETAIL AUCTION BY ID");
            System.out.println("Id:");
            String text = reader.readLine();
            data.put("id", text);
            outToServer.println(data.toString().replace("=",":").substring(1,data.toString().length()-1));
            outToServer.flush();
            data.clear();

            aux_in = inFromServer.readLine();
            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }
            if ("detail_auction".compareTo((String) data.get("type")) == 0) {

                System.out.println("\nTitle: " + data.get("title") + "\nDescription: " + data.get("description") + "\nDeadline: " + data.get("deadline") + "\nCode: " + data.get("code"));

                int mC = Integer.parseInt((String) data.get("messages_count"));
                int bC = Integer.parseInt((String) data.get("bids_count"));

                System.out.println("\nMessages Count: " + mC);

                for (int i = 0; i < mC; i++) {
                    System.out.println("User: " + data.get("messages_" + i + "_user"));
                    System.out.println("Message: " + data.get("messages_" + i + "_text"));
                }

                System.out.println("\nBids Count: " + bC);

                for (int i = 0; i < bC; i++) {
                    System.out.println("User: " + data.get("bids_" + i + "_user"));
                    System.out.println("Amount: " + data.get("bids_" + i + "_amount"));
                }
                System.out.println();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void userActivity(){
        HashMap data = new HashMap();
        String aux_in;
        try {
            data.put("type", "my_auctions");

            outToServer.println(data.toString().replace("=",":").substring(1,data.toString().length()-1));
            outToServer.flush();
            data.clear();

            aux_in = inFromServer.readLine();
            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }

            if ("my_auctions".compareTo((String) data.get("type")) == 0 && "0".compareTo((String)data.get("items_count"))==0) {
                System.out.println("This user has no activity!");
            }else{
                System.out.println("\t\tMy Auctions"+"\nNumber of items:"+Integer.parseInt( (String)data.get("items_count"))+"\n");
                for(int i=0;i<Integer.parseInt( (String)data.get("items_count"));i++){
                    System.out.println("Item:"+i+"\nId:"+data.get("items_"+i+"_id")+"\nCode:"+data.get("items_"+i+"_code")+"\nTitle:"+data.get("items_"+i+"_title")+"\n");}
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeMural(){
        HashMap data = new HashMap();
        String aux_in;
        try {
            data.put("type", "message");
            System.out.println("Auction Id:");
            String text = reader.readLine();
            data.put("id", text);

            System.out.println("\tWrite your message:)");
            text = reader.readLine();

            data.put("text",text);

            outToServer.println(data.toString().replace("=",":").substring(1,data.toString().length()-1));
            outToServer.flush();
            data.clear();

            aux_in = inFromServer.readLine();
            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }

            if ("message".compareTo((String) data.get("type")) == 0 && "true".compareTo((String)data.get("ok"))!=0) {
                System.out.println("You didn't write in the mural with success.");
            }else{
                System.out.println("You wrote with success.");
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void editAuction(){
        HashMap data = new HashMap();
        String aux_in;
        try {
            data.put("type", "edit_auction");
            System.out.println("Auction Id:");
            String text = reader.readLine();
            data.put("id", text);

            System.out.println("\t\tYou can edit:"+"\n1 - Deadline\n\t2-Title\n\t3-Description\n\t4-Amount(Not working yet - Roger Fault)\n\t5-Cancel(Not working yet))\n\tChoose One:");
            text = reader.readLine();
            String action = "";
            if(text.compareTo("1")==0){
                action= "deadline";
                System.out.println("New deadline:");
            }
            else if(text.compareTo("2")==0){
                action = "title";
                System.out.println("New title:");
            }
            else if(text.compareTo("3")==0){
                action = "description";
                System.out.println("New description:");
            } else if (text.compareTo("4") == 0) {
                action = "amount";
                System.out.println("New Amount:");
            }

            text = reader.readLine();

            data.put("action",action);
            data.put(action, text);

            outToServer.println(data.toString().replace("=",":").substring(1,data.toString().length()-1));

            outToServer.flush();
            data.clear();

            aux_in = inFromServer.readLine();
            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }

            if ("edit_auction".compareTo((String) data.get("type")) == 0 && "true".compareTo((String)data.get("ok"))!=0) {
                System.out.println("The auction wasn't edited with success ");
            }else{
                System.out.println("The auction was edited with success!");
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void searchAuction(){
        HashMap data = new HashMap();
        String aux_in;
        try {
            data.put("type", "search_auction");
            System.out.println("Code of the Auction you pretend the details:");
            String text = reader.readLine();
            data.put("code", text);

            outToServer.println(data.toString().replace("=",":").substring(1,data.toString().length()-1));
            outToServer.flush();
            data.clear();

            aux_in = inFromServer.readLine();
            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }

            if ("search_auction".compareTo((String) data.get("type")) == 0 && "0".compareTo((String) data.get("items_count"))==0) {
                System.out.println("No auctions with that code!");
            } else {
                System.out.println("Auction:"+"\nNumber of items:"+ data.get("items_count") +"\n");
                for(int i=0;i<(Integer.parseInt((String) data.get("items_count")));i++){
                    System.out.println("Item:"+i+"\nId:"+data.get("items_"+i+"_id")+"\nCode:"+data.get("items_"+i+"_code")+"\nTitle:"+data.get("items_"+i+"_title")+"\n");}
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void bid(){
        HashMap data = new HashMap();
        String aux_in;
        try{
            data.put("type","bid");
            System.out.println("Make your bid");
            System.out.println("Id:");
            String text = reader.readLine();
            data.put("id",text);
            System.out.println("Amount:");
            text = reader.readLine();
            data.put("amount",text);

            outToServer.println(data.toString().replace("=",":").substring(1,data.toString().length()-1));
            outToServer.flush();
            data.clear();

            aux_in = inFromServer.readLine();
            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }

            if ("bid".compareTo((String) data.get("type")) ==0 && "true".compareTo((String)data.get("ok"))==0) {
                System.out.println("Bid Created.");
            }else
                System.out.println("ERROR CREATE BID");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void onlineUsers(){
        HashMap data = new HashMap();
        String aux_in;
        try{
            data.put("type","online_users");
            outToServer.println(data.toString().replace("=",":").substring(1,data.toString().length()-1));
            outToServer.flush();
            data.clear();

            aux_in = inFromServer.readLine();
            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }

            if ("online_users".compareTo((String) data.get("type")) == 0 && "0".compareTo((String) data.get("users_count"))==0) {
                System.out.println("No users online!");
            } else {
                System.out.println("\nNumber of online users:"+ data.get("users_count"));

            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void cancelAuctionByCode(){
        HashMap data = new HashMap();
        String aux_in;
        try {
            data.put("type", "cancel_auctionByCode");
            System.out.println("Code:");
            String text = reader.readLine();
            data.put("code", text);

            outToServer.println(data.toString().replace("=",":").substring(1,data.toString().length()-1));
            outToServer.flush();
            data.clear();

            aux_in = inFromServer.readLine();
            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }

            if ("cancel_auctionByCode".compareTo((String) data.get("type")) ==0 && "true".compareTo((String)data.get("ok"))==0) {
                System.out.println("Auction Canceled.");
            }else
                System.out.println("ERROR CANCELING AUCTION");

        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void statistics(){
        HashMap data = new HashMap();
        String aux_in;
        try {
            data.put("type", "statistics");
            outToServer.println(data.toString().replace("=",":").substring(1,data.toString().length()-1));
            outToServer.flush();
            data.clear();
            aux_in = inFromServer.readLine();
            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }
            if ("statistics".compareTo((String) data.get("type")) == 0) {
                System.out.println("----------------------");
                System.out.println("TOP 10 USERS WITH MORE CREATED AUCTIONS: \n");
                for (int i = 0; i < Integer.parseInt((String) data.get("sizeMoreBidsUsers")); i++) {
                    System.out.println("User: " + data.get("more_bids_users"+i));
                }
                System.out.println("----------------------");
                System.out.println("AUCTIONS IN THE LAST 10 DAYS: "+(data.get("sizeAuctions10Days"))+"\n");
                for(int i=0; i< Integer.parseInt((String)data.get("sizeAuctions10Days"));i++){
                    System.out.println("Auction: " + data.get("auctions_10_days"+i));
                }
                System.out.println("----------------------");
                System.out.println("TOP 10 USERS WITH MORE WIN AUCTIONS: \n");
                for(int i=0; i<Integer.parseInt((String)data.get("sizeWinnerUsers"));i++){
                    System.out.println("User: " + data.get("winner_users"+i));
                }
                System.out.println("----------------------");
            }else{
                }


        }catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void testServer() {
        HashMap data = new HashMap();
        String aux_in;
        try {
            data.put("type", "testServer");
            outToServer.println(data.toString().replace("=", ":").substring(1, data.toString().length() - 1));
            outToServer.flush();
            data.clear();
            aux_in = inFromServer.readLine();
            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }
            if ("ok".compareTo((String) data.get("test")) ==0){
                System.out.println("SUCCESS!");
            }
            else if("failed".compareTo((String) data.get("test"))==0)
                System.out.println("FAILED!");



        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void banUser(){
        HashMap data = new HashMap();
        String aux_in, userToBan;
        try {
            data.put("type", "banUser");
            System.out.println("User to be banned: ");
            userToBan = reader.readLine();
            data.put("userToBan", userToBan);
            outToServer.println(data.toString().replace("=",":").substring(1,data.toString().length()-1));
            outToServer.flush();
            data.clear();
            aux_in = inFromServer.readLine();
            String[] aux_split_1 = aux_in.split(",");
            for (String e1 : aux_split_1) {
                String[] aux_split_2 = e1.split(":");
                data.put(aux_split_2[0].trim(), aux_split_2[1].trim());
            }
            System.out.println((String)data.get("msg"));


        }catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws IOException {
        // connect to the specified address:port (default is localhost:12345)
        new Client();

    }
}
